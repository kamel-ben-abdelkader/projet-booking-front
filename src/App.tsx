import './index.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import HotelsPage from './pages/HotelsPage';
import RoomsPage from './pages/RoomsPage';
import DashboardAdmin from './pages/DasboardAdmin';
import SignUpPro from './pages/SignUpPro';
import AddHotelPage from './pages/AddHotelPage';
import Footer from './components/Footer';
import AddRoomPage from './pages/AddRoomPage';
import { useGetAccountUserQuery } from './app/user-api';
import ProfilePage from './pages/ProfilePage';
import CustomProfilePage from './pages/CustomProfil';
import HotelOnePage from './pages/HotelOnePage';
import { HotelUpdatePage } from './pages/HotelUpdatePage';
import { RoomUpdatePage } from './pages/RoomUpdatePage';
import RoomOnePage from './pages/RoomOnePage';
import ContactPage from './pages/ContactPage';
import AddBookingPage from './pages/AddBookingPage';
import PaiementPage from './pages/PaiementPage';
import { BookingUpdatePage } from './pages/BookingUpdatePage';


export interface RouterParams{
  id: string
}


function App() {
  useGetAccountUserQuery(undefined, { skip: localStorage.getItem('token') === null });

  return (
    <BrowserRouter>
 <div>

   <Navbar />

 <Routes >

    <Route path="/" element= {<HomePage/>} />
   <Route path="/signup" element= {<SignUp/>} />
   <Route path="/signup-pro" element= {<SignUpPro />} />


   <Route path="/profile" element= {<ProfilePage/>} />
   <Route path="/custom-profile" element= {<CustomProfilePage/>} />

   <Route path="/login" element= {<Login/>} />


   <Route path="/dashboard" element= {<Dashboard/>} />
   <Route path="/dashboard-pro" element= {<DashboardAdmin/>} />

   <Route path="/hotel/:id" element= {<HotelOnePage/>} />
   <Route path="/hotels" element= {<HotelsPage/>} />
   <Route path="/admin/hotel" element= {<AddHotelPage/>} />
   <Route path="/admin/update-hotel/:id" element= {<HotelUpdatePage/>} />

   <Route path="/room/:id" element= {<RoomOnePage/>} />
   <Route path="/rooms" element= {<RoomsPage/>} />
   <Route path="/admin/room/:id" element= {<AddRoomPage/>} />
   <Route path="/admin/update-room/:id" element= {<RoomUpdatePage/>} />

   <Route path="/admin/booking/:id" element= {<AddBookingPage/>} />
   <Route path="/admin/update-booking/:id" element= {<BookingUpdatePage/>} />


   {/* <Route path="/hotel/:id" element= {<HotelDetailPage/>} /> */}



   <Route path="/contact" element= {<ContactPage/>} />

   <Route path="/paiement/:id" element= {<PaiementPage/>} />
     



      

</Routes>
<Footer />
</div>
  

    </BrowserRouter>
  );
}

export default App;
