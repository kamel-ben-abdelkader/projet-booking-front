import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Hotel, Room } from '../entities';
import { prepare } from './token';


export const hotelAPI = createApi({
    reducerPath: 'hotelAPI',
    tagTypes: ['HotelList', 'HotelUser'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/hotel', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllHotels: builder.query<Hotel[], void>({
            query: () =>  '/',
            providesTags: ['HotelList']
        }),
        getOneHotel: builder.query<Hotel, number>({
            query: (id) => '/'+id,
            providesTags: ['HotelList']
        }),
        getHotelsbyUserId: builder.query<Hotel[], number>({
            query: (id) => '/user/'+id,
            providesTags: ['HotelList', 'HotelUser']

        }),

        getHotelRoomsbyUserId: builder.query<Room[], number>({
            query: (id) => '/test/'+id,
            providesTags: ['HotelList', 'HotelUser']
        }),

        getSearchedHotel: builder.query<Hotel[], any>({
            query: (val) => ({
                url: '/search/'+ val,
                method: 'GET',
                
            }),
           
        }),
        postHotel: builder.mutation<Hotel, Hotel>({
            query: (body) => {
                const formData = new FormData();
                formData.append('name', body.name!);
                formData.append("description", body.description!);
                formData.append("adress", body.adress!);
                formData.append("service",body.service!);
                formData.append("type", body.type!);
                formData.append("picture", body.picture! );
                return {
                  url: '/',
                  method: 'POST',
                  body: formData
                }
            },
            invalidatesTags: ['HotelList']
        }),


        patchHotel: builder.mutation< Hotel, {body: Hotel, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['HotelList']
        }),
        deleteHotel: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['HotelList']
        })

    })
});


export const {useGetAllHotelsQuery, useGetHotelRoomsbyUserIdQuery, useGetSearchedHotelQuery, usePostHotelMutation, usePatchHotelMutation, useDeleteHotelMutation, useGetHotelsbyUserIdQuery, useGetOneHotelQuery} = hotelAPI;


