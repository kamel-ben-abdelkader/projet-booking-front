import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Booking, Room } from '../entities';
import { prepare } from './token';


export const bookingAPI = createApi({
    reducerPath: 'bookingAPI',
    tagTypes: ['BookingList', 'BookingUser'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/booking', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllBookings: builder.query<Booking[], void>({
            query: () =>  '/',
            providesTags: ['BookingList']
        }),
        getOneBooking: builder.query<Booking, number>({
            query: (id) => '/'+id,
            providesTags: ['BookingList']
        }),
        getBookingsbyUserId: builder.query<Booking[], number>({
            query: (id) => '/user/'+id,
            providesTags: ['BookingList', 'BookingUser']

        }),

        getBookingsbyRoomId: builder.query<Room[], number>({
            query: (id) => '/room/'+id,
            providesTags: ['BookingList', 'BookingUser']
        }),

        getSearchedBooking: builder.query<Booking[], any>({
            query: (val) => ({
                url: '/search/'+ val,
                method: 'GET',
                
            }),
           
        }),
        
        postBooking: builder.mutation<Booking, Booking>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags:['BookingList']
        }),

    //     postBooking: builder.mutation<Booking, Booking>({
    //         query: (body) => {
    //             const formData = new FormData();
    //             formData.append('arrivalDate', body.arrivalDate!.toString());
    //             formData.append("departureDate", body.departureDate!.toString());
    //             formData.append("status", body.status!.toString());
    //             formData.append("customizedPrice",body.customizedPrice!.toString());
    //             formData.append("userId", body.userId!.toString() );
    //             formData.append("roomId", body.roomId!.toString());
    //             return {
    //             url: '/',
    //             method: 'POST',
    //             body: formData
    //         }
    //     },
    //     invalidatesTags: ['BookingList']
    // }),







        patchBooking: builder.mutation< Booking, {body: Booking, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['BookingList']
        }),
        deleteBooking: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['BookingList']
        })

    })
});


export const {useGetAllBookingsQuery, useGetBookingsbyRoomIdQuery, useGetSearchedBookingQuery, usePostBookingMutation, usePatchBookingMutation, useDeleteBookingMutation, useGetBookingsbyUserIdQuery, useGetOneBookingQuery} = bookingAPI;


