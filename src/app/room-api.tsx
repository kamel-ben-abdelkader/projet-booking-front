import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Room } from '../entities';
import { prepare } from './token';


export const roomAPI = createApi({
    reducerPath: 'roomAPI',
    tagTypes: ['RoomList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/room', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllRooms: builder.query<Room[], void>({
            query: () =>  '/',
            providesTags: ['RoomList']
        }),
        getOneRoom: builder.query<Room, number>({
            query: (id) => '/'+id,
            providesTags: ['RoomList']
            
        }),

        getOneRoombyHotelId: builder.query<Room[], number>({
            query: (id) => '/hotel/'+id,
            providesTags: ['RoomList']

        }),

        getSearchedRoom: builder.query<Room[], any>({
            query: (val) => ({
                url: '/search/'+ val,
                method: 'GET',
                invalidatesTags: ['RoomList']
            }),
        
           
        }),
        postRoom: builder.mutation<Room, Room>({
            query: (body) => {
                const formData = new FormData();
                formData.append('name', body.name!);
                formData.append("description", body.description!);
                formData.append("location", body.location!);
                formData.append("price",body.price!);
                formData.append("picture", body.picture! );
                formData.append("hotelId", body.hotelId!.toString());
                return {
                url: '/',
                method: 'POST',
                body: formData
            }
        },
        invalidatesTags: ['RoomList']
    }),


        patchRoom: builder.mutation< Room, {body: Room, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['RoomList']
        }),
        deleteRoom: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['RoomList']
        })

    })
});


export const {useGetAllRoomsQuery, useGetSearchedRoomQuery, usePostRoomMutation, usePatchRoomMutation, useDeleteRoomMutation, useGetOneRoomQuery,  useGetOneRoombyHotelIdQuery} = roomAPI;


