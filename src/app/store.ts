import { bookingAPI } from './booking-api';
import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { userAPI } from './user-api';
import authSlice from './auth-slice';
import { hotelAPI } from './hotel-api';
import { roomAPI } from './room-api';

export const store = configureStore({
  reducer: {
    [userAPI.reducerPath]:userAPI.reducer,
    [hotelAPI.reducerPath]:hotelAPI.reducer,
    [roomAPI.reducerPath]:roomAPI.reducer,
    [bookingAPI.reducerPath]:bookingAPI.reducer,


    auth:authSlice,

  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat( userAPI.middleware, hotelAPI.middleware,  roomAPI.middleware,  bookingAPI.middleware),
});


export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;


