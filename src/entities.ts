export interface User{
    id?:number;
    firstname?:string;
    name?:string;
    company?:string;
    email?:string;
    password?:string;
    adress?: string;
    phone?: number;
    role?: string;
    siret?:number;
}

export interface Hotel {
    id?:number;
    name?:string; 
    description?:string; 
    adress?:string; 
    service?:string;
    type?:string; 
    picture?:string; 
    users?:User;
}

export interface Room {
    id?:number;
    name?:string; 
    description?:string; 
    location?:string;
    price?:string;
    picture?:string;
    hotelId?:number
}



export interface Booking {
    id?:number;
    arrivalDate?:Date; 
    departureDate?:Date; 
    status?:number;
    customizedPrice?:number;
    userId?:number;
    roomId?:number
}


// export interface Product {
//     id?:number;
//     name?:string;
//     brand?:string;
//     description?:string;
//     price?:number;
//     picture?:string;
//     stock?:number;
//     categories?:Category[];
//     productlines?:ProductLine[];
// }

// export interface Category {
//     id?:number;
//     label?:string;
//     products?:Product[];
// }
