import { useGetAllRoomsQuery } from "../../app/room-api";
import { Room } from "../../entities";
import SelectLocation from "../SelectLocation";
import HomeRoomsCard from "./HomeRoomsCard";
import { Fragment, useState } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'
import { useGetSearchedRoomQuery } from '../../app/room-api'

const cities = [
  {
    id: 1,
    location: 'Paris',

  },
  {
    id: 2,
    location: 'Lyon',
  },
  {
    id: 3,
    location: 'Marseille',

  },
]

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

export default function HomeRoomsList() {

  const [selected, setSelected] = useState(cities[2])


  const [searchedVal, setSearchedVal] = useState(" ")
  const roomSearchQuery = useGetSearchedRoomQuery(searchedVal);
  const roomAllQuery = useGetAllRoomsQuery();
  

  return (

    <div className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto max-w-7x1">
        <div className="flex flex-wrap w-full mb-4 p-4">
          <div className="w-full mb-6 lg:mb-0">
            <h1 className="sm:text-4xl text-5xl font-medium font-bold title-font mb-2 text-gray-900">Chambres d'Hôtels</h1>
            <div className="h-1 w-20 bg-indigo-500 rounded">
            </div>
          </div>
        </div>

        <Listbox value={selected} onChange={setSelected}>
          {({ open }) => (
            <>
              <Listbox.Label className="block text-sm font-medium text-gray-700">Choissez une ville</Listbox.Label>
              <div className="mt-1 relative">
                <Listbox.Button className="relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                  <span className="flex items-center">

                    <span className="ml-3 block truncate">{selected.location}</span>
                  </span>
                  <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                    <SelectorIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                  </span>
                </Listbox.Button>

                <Transition
                  show={open}
                  as={Fragment}
                  leave="transition ease-in duration-100"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                    {cities.map((city) => (
                      <Listbox.Option
                        key={city.id}
                        className={({ active }) =>
                          classNames(
                            active ? 'text-white bg-indigo-600' : 'text-gray-900',
                            'cursor-default select-none relative py-2 pl-3 pr-9'
                          )
                        }
                        value={city}
                      >
                        {({ selected, active }) => (
                          <>
                            <div className="flex items-center">

                              <span
                                className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}
                              >
                                <button onClick={() => setSearchedVal(city.location)}>
                                  {city.location}
                                  {/* {searchedVal} */}
                                </button>

                              </span>
                            </div>

                            {selected ? (
                              <span
                                className={classNames(
                                  active ? 'text-white' : 'text-indigo-600',
                                  'absolute inset-y-0 right-0 flex items-center pr-4'
                                )}
                              >
                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                              </span>
                            ) : null}
                          </>
                        )}
                      </Listbox.Option>
                    ))}
                  </Listbox.Options>
                </Transition>
              </div>
            </>
          )}
        </Listbox>


        {/*       
      <SelectLocation /> */}
        <div className="flex flex-wrap -m-4">

          {roomSearchQuery.data ? roomSearchQuery.data!.map((item: Room) => (

            <HomeRoomsCard key={item.id} room={item} />
          )) :
            roomAllQuery.data?.map((item: Room) => (

              <HomeRoomsCard key={item.id} room={item} />
            ))}


        </div>
      </div>
    </div>


  )
}