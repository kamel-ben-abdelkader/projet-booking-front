
import { Link } from "react-router-dom";
import { useGetBookingsbyRoomIdQuery, useGetBookingsbyUserIdQuery } from "../../app/booking-api";
import { useAppSelector } from "../../app/hooks";
import { Booking, Room } from "../../entities";
import BookingAvailablity from "../Bookings/BookingAvailibility";
import Availablity from "./Availablity";


export default function HomeRoomsCard(props: { room: Room; }) {



  const { room } = props;

  let user = useAppSelector(state => state.auth.user)



  const { data } = useGetBookingsbyRoomIdQuery(Number(room?.id));
  console.log(data);
  console.log(room.id);


  function getSrc(picture: any) {
    if (!picture) {
      return 'https://via.placeholder.com/600'
    }
    else if (picture.startsWith('http')) {

      return picture

    }
    else return process.env.REACT_APP_SERVER_URL + picture

  }


  return (



    <div className="xl:w-1/3 md:w-1/2 p-4">
      <div className="bg-white p-6 rounded-lg">
        <Link to={'/room/' + room.id}>
          <img className="lg:h-60 xl:h-56 md:h-64 sm:h-72 xs:h-72 h-72  rounded w-full object-cover object-center mb-6" src={getSrc(room.picture)} />

        </Link>
        <h2 className="text-lg text-gray-900 font-medium text-center title-font mb-4">{room.name}</h2>
        {/* <h3 className="tracking-widest text-indigo-500 text-m font-medium title-font">{data?.map((item: Booking) => (

          <BookingAvailablity key={item.id} booking={item} />
        ))} la nuit au lieu de {room.price} €</h3> */}
        {/* <h2 className="text-lg text-gray-900 font-medium title-font mb-4">{room.name}</h2> */}
        <p className="leading-relaxed text-center">{room.location}</p>

      </div>
    </div>

  )
}