

import { Link } from "react-router-dom";
import { useGetBookingsbyUserIdQuery } from "../../app/booking-api";
import { useAppSelector } from "../../app/hooks";
import { Booking, Room } from "../../entities";


export default function Availablity(props: { booking: Booking; }) {

    

    const { booking } = props;

    let user = useAppSelector(state => state.auth.user)

    
  return(



          <div className="xl:w-1/3 md:w-1/2 p-4">
        <p className="leading-relaxed">Date arrivée {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.arrivalDate!))}</p>
        <p className="leading-relaxed">Date de {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.departureDate!))}</p>
      
        </div>
     
        
  )
}