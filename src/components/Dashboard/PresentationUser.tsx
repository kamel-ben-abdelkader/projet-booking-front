import { useAppSelector } from "../../app/hooks";

export const PresentationUser = () => {

    let user = useAppSelector(state => state.auth.user)
    var ladate=new Date()

    return (
      <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
        <div className="max-w-xl mb-5 md:mx-auto sm:text-center lg:max-w-2xl">
          <div className="mb-4">
            <h2
              className="inline-block max-w-lg font-sans text-3xl font-extrabold leading-none tracking-tight text-black transition-colors duration-200 hover:text-deep-purple-accent-700 sm:text-4xl"
            >
              Bonjour et bienvenue
            </h2>
          </div>
          <p className="mb-2 text-xs font-semibold tracking-wide text-gray-600 uppercase sm:text-center">
        {ladate.getDate()+"/"+(ladate.getMonth()+1)+"/"+ladate.getFullYear()}
        </p>
        <div className="mb-10 sm:text-center">
          <div className="inline-block mb-1">
            <img
              alt="avatar"
              src="https://business.ucr.edu/sites/g/files/rcwecm2116/files/styles/form_preview/public/icon-individual.png?itok=wbbnrR9I"
              className="object-cover w-10 h-10 rounded-full shadow-sm"
            />
          </div>
          <div>
            <p
              className="font-semibold text-gray-800 transition-colors duration-200 hover:text-deep-purple-accent-700"
            >
              {user?.firstname} {user?.name}
            </p>
          </div>
        </div>
          <p className="text-base text-gray-700 md:text-lg">
            Dans cet espace vous pouvez accéder à l'ensemble de vos achats et de vos réservations.
          </p>
        </div>
        {/* <div className="sm:text-center">
          <a
            href="/"
            aria-label=""
            className="inline-flex items-center font-semibold transition-colors duration-200 text-deep-purple-accent-400 hover:text-deep-purple-800"
          >
            Read more
          </a>
        </div> */}
      </div>
    );
  };