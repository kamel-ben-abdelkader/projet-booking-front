import { useNavigate } from "react-router"
import { Link } from "react-router-dom";
import { useAppSelector } from "../../app/hooks"

  export default function AddHotel() {
    const navigate = useNavigate()


    let user = useAppSelector(state => state.auth.user)

    

    return (
     
        
            <header id="up" className="bg-center bg-fixed bg-no-repeat bg-center bg-cover h-screen relative">
               
                <div className="h-screen bg-opacity-50 bg-black flex items-center justify-center" >
                    <div className="mx-2 text-center">
                        <h1 className="text-gray-100 font-extrabold text-4xl xs:text-5xl md:text-6xl">
                            <span className="text-white">Bienvenue</span> {user?.firstname}
                   </h1>
                   <h2 className="text-gray-200 font-extrabold text-3xl xs:text-4xl md:text-5xl leading-tight">
                    Ajouter <span className="text-white">vos</span> Hotels <span className="text-white">et vos </span> Chambres
                   </h2>
                   <div className="inline-flex">


                   <Link to="/admin/hotel">
                   <button className="p-2 my-5 mx-2 bg-transparent border-2 bg-indigo-200 bg-opacity-75 hover:bg-opacity-100 border-indigo-700 rounded hover:border-indigo-800 font-bold text-indigo-800 shadow-md transition duration-500 md:text-lg">Ajouter</button>
                    </Link>
              
                   </div>
                </div>
            </div>
        </header>
      
    
    )
  }
  
  