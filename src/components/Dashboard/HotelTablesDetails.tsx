import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { hotelAPI } from "../../app/hotel-api";
import { Hotel } from "../../entities";





  
  export default function HotelTablesDetails  (props: { hotel: Hotel; }) {
      
    let user = useAppSelector(state => state.auth.user)


  function getSrc(picture: any): any {
    if (!picture)
     { 
      return  'https://via.placeholder.com/600'
     }
     else if (picture.startsWith('http')) {
    
     return picture 
       
     }
     else return process.env.REACT_APP_SERVER_URL + picture 
     
    
     }

    const { hotel } = props; 

    return (

<tr>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
        <div className="flex items-center">
        <Link to={"/hotel/" + hotel!.id}>
            <div className="flex-shrink-0 w-10 h-10">
                <img className="w-full h-full rounded-full"
                src={getSrc(hotel.picture)}
                     />
            </div>
            </Link>
         
        </div>
    </td>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <Link to={"/hotel/" + hotel!.id}>
    <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">
            {hotel.name}</span>
                        
        </span>
      
        </Link>
    </td>
   
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <Link to={"/admin/update-hotel/" + hotel!.id}>
        <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">
                          Edit</span>
                        
        </span>
        </Link>
    </td>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <Link to={"/admin/room/" + hotel!.id}>
        <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">
                          Ajouter Chambre </span>
                        
        </span>
        </Link>
    </td>
</tr>

    )}