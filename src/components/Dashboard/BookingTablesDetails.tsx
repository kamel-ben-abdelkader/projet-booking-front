import { Link } from "react-router-dom";
import { useAppSelector } from "../../app/hooks";
import { useGetOneRoomQuery } from "../../app/room-api";
import {  Booking } from "../../entities";





  
  export default function BookingTablesDetails  (props: { booking: Booking; }) {
    let user = useAppSelector(state => state.auth.user)

    const { booking } = props; 

    const roomQuery = useGetOneRoomQuery(Number(booking.roomId))

    function getStatus(status: Number) {
        if (status===0)
         { 
          return  'en ligne'
         }
     
         else return "hors ligne";
        
         }

    return (booking.id ?

<tr>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">
            {roomQuery.data?.name} </span>
                        
        </span>
    </td>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <Link to={"/booking/" + booking!.id}>
    <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">Du {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.arrivalDate!))} au {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.departureDate!))}</span>
                        
        </span>
      
        </Link>
    </td>
   
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
    <Link to={"/admin/update-booking/" + booking!.id}>
        <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
            <span className="relative text-indigo-600 hover:text-indigo-900">
                          Edit</span>
                        
        </span>
        </Link>
    </td>
    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
        <span
            className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
            <span aria-hidden
                className="absolute inset-0 bg-green-500 rounded-full"></span>
            <span className="relative text-white hover:text-indigo-900">
            {getStatus(booking!.status!)} </span>
                        
        </span>
     
    </td>
</tr> : <p></p>

    )}