import { useGetBookingsbyUserIdQuery } from "../../app/booking-api";
import { useAppSelector } from "../../app/hooks";
import {useGetHotelRoomsbyUserIdQuery } from "../../app/hotel-api";
import {Booking, Room } from "../../entities";
import BookingTablesDetails from "./BookingTablesDetails";
import RoomTablesDetails from "./RoomTablesDetails";



export default function BookingTables() {

    let user = useAppSelector(state => state.auth.user)
    // const { data, isLoading, isError } = useGetAllHotelsQuery();
    const { data } = useGetBookingsbyUserIdQuery(Number(user?.id));
    return (

        <div className="antialiased font-sans bg-gray-200">
            <div className="container mx-auto px-4 sm:px-8">
                <div className="py-8">
                    <div>
                        <h2 className="text-2xl font-semibold leading-tight">Booking</h2>
                    </div>

                    <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                        <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                            <table className="min-w-full leading-normal">
                                <thead>
                                    <tr>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Nom de la chambre
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Disponibilité
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Modifier
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Status
                                        </th>
                                    </tr>
                                </thead>
                                {data?.map((item: Booking) => (


                                    <BookingTablesDetails key={item.roomId} booking={item} />

                                ))}

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
