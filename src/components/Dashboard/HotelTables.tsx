import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {hotelAPI, useGetHotelsbyUserIdQuery } from "../../app/hotel-api";
import { Hotel } from "../../entities";
import HotelTablesDetails from "./HotelTablesDetails";



export default function HotelTables() {

    let user = useAppSelector(state => state.auth.user)
    // const { data, isLoading, isError } = useGetAllHotelsQuery();
    const { data } = useGetHotelsbyUserIdQuery(Number(user?.id));
 
    return (

        <div className="antialiased font-sans bg-gray-200">
            <div className="container mx-auto px-4 sm:px-8">
                <div className="py-8">
                    <div>
                        <h2 className="text-2xl font-semibold leading-tight">Hotels</h2>
                    </div>

                    <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                        <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                            <table className="min-w-full leading-normal">
                                <thead>
                                    <tr>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Hotel
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Nom
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Modifier
                                        </th>
                                        <th
                                            className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Ajouter
                                        </th>
                                    </tr>
                                </thead>
                                {data?.map((item: Hotel) => (


                                    <HotelTablesDetails key={item.id} hotel={item} />

                                ))}

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
