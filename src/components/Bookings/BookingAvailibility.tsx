

import { Link } from "react-router-dom";
import { useGetBookingsbyUserIdQuery } from "../../app/booking-api";
import { useAppSelector } from "../../app/hooks";
import { Booking, Room } from "../../entities";


export default function BookingAvailablity(props: { booking: Booking; }) {

    

    const { booking } = props;

    let user = useAppSelector(state => state.auth.user)

    
  return(
   
    <span className="title-font font-medium text-2xl text-gray-900">{booking.customizedPrice} € / Nuit</span>
        
  )
}