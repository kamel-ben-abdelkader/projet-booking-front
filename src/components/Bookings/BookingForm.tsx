import { useState } from "react";
import { useNavigate, useParams } from "react-router";
import { bookingAPI, usePostBookingMutation } from "../../app/booking-api";
import { useAppDispatch, useAppSelector } from "../../app/hooks";

import { Booking } from "../../entities";





export default function BookingForm() {

    const { id } = useParams();

    const navigate = useNavigate()

    let user = useAppSelector(state => state.auth.user)

    const [postBooking, postQuery] = usePostBookingMutation();
    const [form, setForm] = useState<Booking>({} as Booking);
    let dispatch = useAppDispatch()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        form.roomId= Number(id);
        event.preventDefault();
        try {
            const data = await postBooking(form).unwrap()
            dispatch(bookingAPI.util.invalidateTags(['BookingList']))
             alert("Vos dates de réservations ont bien été enregistrées")
             navigate('/dashboard-pro')
 
 
         } catch (error) {
             event.preventDefault();
             alert("Vos modifications sont erronnées, recommencez")
             console.log(error);
 
         }
       
     }
        
   


    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }

    return (

        <div className="bg-white font-family-karla 	min-height: 100vh">

            <div className="w-full flex flex-wrap">

                <div className="w-full md:w-1/2 flex flex-col">

                    <div className="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-12">
                        <a href="#" className="bg-black text-white font-bold text-xl p-4">L B</a>
                    </div>

                    <div className="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
                        <p className="text-center text-3xl">Ajouter une Reservation</p>
                        <form className="flex flex-col pt-3 md:pt-8" onSubmit={handleSubmit} >

                            <div className="flex flex-col pt-4">
                                <label htmlFor="name" className="text-lg">Date arrivée</label>
                                <input
                                    type="date"
                                    name="arrivalDate"
                                    id="arrivalDate"
                                    autoComplete="arrivalDate"
                                    placeholder="nom de l'Hotel"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="flex flex-col pt-4">
                                <label htmlFor="departureDate" className="text-lg">
                                    date depart
                                </label>
                                <input
                                    type="date"
                                    id="departureDate"
                                    name="departureDate"
                                    placeholder="departureDate"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>


                            <div className="flex flex-col pt-4">
                                <label htmlFor="customizedPrice" className="text-lg">
                                    Prix de la nuit : 
                                </label>
                                <input
                                    type="number"
                                    id="customizedPrice"
                                    name="customizedPrice"
                                    placeholder="prix"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>
                       

                            <input type="submit"
                                value="Sauvegarder"
                                className=" bg-blue-800 text-white font-bold text-lg hover:bg-blue-400 p-2 mt-8 mb-10"

                            />
                        </form>

                    </div>

                </div>


                <div className="w-1/2 shadow-2xl ">
                    <img className="object-cover w-full h-full hidden md:block" src="https://images.unsplash.com/photo-1445991842772-097fea258e7b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="Background" />
                </div>
            </div>

        </div>


    )
}
