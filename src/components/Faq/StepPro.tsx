import { Link } from "react-router-dom";
import { useAppSelector } from "../../app/hooks";

export default function StepPro() {

  let user = useAppSelector(state => state.auth.user)

  return (
    <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
      <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">

        <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
          <span className="relative inline-block">

            <span className="relative">Bienvenue</span>
          </span>{' '}
          {user?.firstname} {user?.name}
        </h2>
        <p className="text-base text-gray-700 md:text-lg">
  Merci pour votre confiance , vous trouverez ci-dessous les étapes pour mettre en ligne vos réservations
        </p>
      </div>
      <div className="relative grid gap-8 row-gap-5 mb-8 md:row-gap-8 lg:grid-cols-4 sm:grid-cols-2">
        <div className="absolute inset-0 flex items-center justify-center sm:hidden lg:flex">
          <div className="w-px h-full bg-gray-300 lg:w-full lg:h-px" />
        </div>
        <div className="p-5 duration-300 transform bg-white border rounded shadow-sm hover:-translate-y-2 hover:bg-indigo-50">
          <div className="flex items-center justify-between mb-2">
            <p className="text-lg font-bold leading-5">Hotel</p>
            <p className="flex items-center justify-center w-6 h-6 font-bold rounded text-deep-purple-accent-400 bg-indigo-50">
              1
            </p>
          </div>
          <p className="text-sm text-gray-900">
           Cliquez sur le bouton ajouter Hotel pour ajouter votre Hotel.
          </p>
        </div>
        <div className="p-5 duration-300 transform bg-white border rounded shadow-sm hover:-translate-y-2 hover:bg-indigo-50">
          <div className="flex items-center justify-between mb-2">
            <p className="text-lg font-bold leading-5">Chambres</p>
            <p className="flex items-center justify-center w-6 h-6 font-bold rounded text-deep-purple-accent-400 bg-indigo-50">
              2
            </p>
          </div>
          <p className="text-sm text-gray-900 ">
          Dans la section Hotel, cliquez sur le bouton ajouter une chambre de votre Hotel pour ajouter votre chambre.
          </p>
        </div>
        <div className="p-5 duration-300 transform bg-white border rounded shadow-sm hover:-translate-y-2 hover:bg-indigo-50">
          <div className="flex items-center justify-between mb-2">
            <p className="text-lg font-bold leading-5">Offres Réservation</p>
            <p className="flex items-center justify-center w-6 h-6 font-bold rounded text-deep-purple-accent-400 bg-indigo-50 ">
              3
            </p>
          </div>
          <p className="text-sm text-gray-900">
          Dans la section Chambre, cliquez sur le bouton ajouter une Réservation de votre Chambre pour ajouter votre réservation.
          </p>
        </div>
        <div className="p-5 duration-300 transform bg-white border rounded shadow-sm hover:-translate-y-2 hover:bg-indigo-50">
          <div className="flex items-center justify-between mb-2">
            <p className="text-lg font-bold leading-5">Publier vos offres</p>
            <p className="flex items-center justify-center w-6 h-6 font-bold rounded text-deep-purple-accent-400 bg-indigo-50">
              4
            </p>
          </div>
          <p className="text-sm text-gray-900">
            Dans la partie Publication, activer ou désactiver vos offres.
          </p>
        </div>
      </div>
      <div className="text-center">
        <Link to="/admin/hotel">
          <button className="p-2 my-5 mx-2 bg-transparent border-2 bg-indigo-200 bg-opacity-75 hover:bg-opacity-100 border-indigo-700 rounded hover:border-indigo-800 font-bold text-indigo-800 shadow-md transition duration-500 md:text-lg">Ajouter un Hotel</button>
        </Link>
      </div>
    </div>
  );
};