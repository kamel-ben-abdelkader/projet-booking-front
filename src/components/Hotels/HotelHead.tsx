import { useParams } from "react-router";
import { useGetAllHotelsQuery, useGetOneHotelQuery } from "../../app/hotel-api";
import { Hotel } from "../../entities";
import HotelDetails from "./HotelDetails";




export default function HotelHead(props: { hotel: Hotel; }) {


    const { hotel } = props;

const { data, isLoading, isError } = useGetAllHotelsQuery();

  return (
 

<div className="container mx-auto my-5">

<div className="relative rounded-lg flex flex-col md:flex-row items-center md:shadow-xl md:h-72 mx-2">
    
    <div className="z-0 order-1 md:order-2 relative w-full md:w-2/5 h-80 md:h-full overflow-hidden rounded-lg md:rounded-none md:rounded-r-lg " 	style={{
	    backgroundImage: `url(${hotel!.picture!.startsWith('http') ? hotel!.picture : process.env.REACT_APP_SERVER_URL! + hotel!.picture})`,
        }}>
        <div className="absolute inset-0 w-full h-full object-fill object-center bg-blue-400 bg-opacity-30 bg-cover bg-bottom"></div>
        <div className="md:hidden absolute inset-0 h-full p-6 pb-6 flex flex-col-reverse justify-start items-start bg-gradient-to-b from-transparent via-transparent to-gray-900">
            <h3 className="w-full font-bold text-2xl text-white leading-tight mb-2">HOTEL AMANEE</h3>
            <h4 className="w-full text-xl text-gray-100 leading-tight">Bienvenido a</h4>
        </div>
        <svg className="hidden md:block absolute inset-y-0 h-full w-24 fill-current text-white -ml-12" viewBox="0 0 100 100" preserveAspectRatio="none">
            <polygon points="50,0 100,0 50,100 0,100" />
        </svg>
    </div>

</div>
</div>
  )
}
