
import { Link } from "react-router-dom";
import { Hotel } from "../../entities";




export default function HotelDetails(props: { hotel: Hotel; }) {


const { hotel } = props;

  return (

  //  <div className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col">
  //               <Link to = {'/hotel/'+ hotel.id}>
  //                   <img className="hover:grow hover:shadow-lg"src={hotel!.picture!.startsWith('http') ? hotel!.picture : process.env.REACT_APP_SERVER_URL! + hotel!.picture}/>
  //                   <div className="pt-3 flex items-center justify-between">
  //                       <p className="">{hotel.name}</p>
  //                       <p className="">{hotel.description}</p>
  //                       {/* <p className="">{hotel.adress}</p> */}
  //                       <p className="">{hotel.service}</p>
  //                       <p className="">{hotel.type}</p>



                     
  //                   </div>
  //                   <p className="pt-1 text-gray-900">{hotel.adress}</p>
  //                   </Link>
  //           </div>
  
            
           
         <div className="w-full md:w-1/2 xl:w-1/3 px-4">
         <div className="bg-white rounded-lg overflow-hidden mb-10">
            
         <Link to = {'/hotel/'+ hotel.id}>
         <img className="w-full transform transition duration-500 hover:scale-110" src={hotel!.picture!.startsWith('http') ? hotel!.picture : process.env.REACT_APP_SERVER_URL! + hotel!.picture}/>
         </Link>
            <div className="p-8 sm:p-9 md:p-7 xl:p-9 text-center">
               <h3
    
                     className="
                     font-semibold
                     text-dark text-xl
                     sm:text-[22px]
                     md:text-xl
                     lg:text-[22px]
                     xl:text-xl
                     2xl:text-[22px]
                     mb-4
                     block
                     hover:text-primary
                     "
                     >
               {hotel.name}
               
               </h3>
              {/* <p className="text-base text-body-color leading-relaxed mb-7">
               {hotel.description}
               </p>
              <p className="">Service : {hotel.service}  </p>
              <p className="">Type :  {hotel.type}    </p>
              <p className="">Adresse : {hotel.adress}   </p> */}

                        {/* <Link to = {'/hotel/'+ hotel.id}>
                
                   <p className="
                  inline-block
                  py-2
                  px-7
                  border border-[#E5E7EB]
                  rounded-full
                  text-base text-body-color
                  bg-blue-500
                  font-medium
                  hover:border-primary hover:bg-primary hover:text-white
                  transition
                  ">Découvrir</p>
                   </Link> */}
              
            </div>
         </div>
      </div>

            
  )
}
