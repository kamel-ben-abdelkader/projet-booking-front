import { useParams } from "react-router";
import { useGetAllHotelsQuery, useGetOneHotelQuery } from "../../app/hotel-api";
import { Hotel } from "../../entities";
import HotelDetails from "./HotelDetails";




export default function HotelLists() {
//     const { id } = useParams();
// console.log (id)


// const { data,  } = useGetOneHotelQuery(Number(id));

const { data, isLoading, isError } = useGetAllHotelsQuery();

  return (
    <>
  <section className="bg-white py-8">

      <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
          {data?.map((item: Hotel) => (
            

              <HotelDetails key={item.id} hotel={item}/>
           
          ))}
        </div>

</section>   
    </>
  )
}
