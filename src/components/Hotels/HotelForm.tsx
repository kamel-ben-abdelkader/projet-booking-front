import { useState } from "react";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { usePostHotelMutation } from "../../app/hotel-api";
import { Hotel, User } from "../../entities";





export default function HotelForm() {

    const navigate = useNavigate()


    let user = useAppSelector(state => state.auth.user)

    const [postHotel, postQuery] = usePostHotelMutation();
    const [form, setForm] = useState<Hotel>({} as Hotel);
    let dispatch = useAppDispatch()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
      


        event.preventDefault();
        try {
           await postHotel(form).unwrap()
            alert("Votre Hotel a bien été rajouter , vous pous pouvez ajouter votre Chambre")
            navigate('/dashboard-pro')




        } catch (error) {
            event.preventDefault();
            alert("Vos modifications sont erronnées, recommencez")
            console.log(error);

        }
      
    }


    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }


    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }



    return (

        <div className="bg-white font-family-karla 	min-height: 100vh">

            <div className="w-full flex flex-wrap">

                <div className="w-full md:w-1/2 flex flex-col">

                    <div className="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-12">
                        <a href="#" className="bg-black text-white font-bold text-xl p-4">L B</a>
                    </div>

                    <div className="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
                        <p className="text-center text-3xl">Ajouter votre Hotel</p>
                        <form className="flex flex-col pt-3 md:pt-8" onSubmit={handleSubmit} >

                            <div className="flex flex-col pt-4">
                                <label htmlFor="name" className="text-lg">Nom Hotel</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    autoComplete="name"
                                    placeholder="nom de l'Hotel"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="flex flex-col pt-4">
                                <label htmlFor="description" className="text-lg">
                                    description
                                </label>
                                <textarea
                                    className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-1.5
                                    text-base
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border border-solid border-gray-300
                                    rounded
                                    transition
                                    ease-in-out
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                    id="description"
                                    name="description"
                                    onChange={handleChange}
                                ></textarea>
                            </div>


                            <div className="flex flex-col pt-4">
                                <label htmlFor="adress" className="text-lg">
                                    adresse
                                </label>
                                <input
                                    type="adress"
                                    id="adress"
                                    name="adress"
                                    placeholder="adress"
                                    autoComplete="adress"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>


                            <div className="flex flex-col pt-4">
                                <label htmlFor="service" className="text-lg">
                                    service
                                </label>
                                <input
                                    type="service"
                                    id="service"
                                    name="service"
                                    placeholder="service"
                                    autoComplete="service"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>


                            <div className="flex flex-col pt-4">
                                <label htmlFor="type" className="text-lg">
                                    type
                                </label>
                                <input
                                    type="type"
                                    id="type"
                                    name="type"
                                    placeholder="type"
                                    autoComplete="type"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="min-height: max-content">

                                <label className="block text-sm font-medium text-black">
                                    Image
                                </label>
                                <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                    <div className="space-y-1 text-center ">
                                        <svg className="mx-auto h-12 w-12 text-black" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        </svg>
                                        <div className="flex text-sm text-gray-600 ">
                                            <label className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                <span className="">Upload a file</span>
                                                <input id="file-upload"
                                                    name="picture"
                                                    type="file"
                                                    className="sr-only"
                                                    required
                                                    onChange={handleFile}
                                                />
                                            </label>
                                            <p className="pl-1 text-black">or drag and drop</p>
                                        </div>
                                        <p className="text-xs text-black">
                                            PNG, JPG
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <input type="submit"
                                value="Sauvegarder"
                                className=" bg-blue-800 text-white font-bold text-lg hover:bg-blue-400 p-2 mt-8 mb-10"

                            />
                        </form>

                    </div>

                </div>


                <div className="w-1/2 shadow-2xl ">
                    <img className="object-cover w-full h-full hidden md:block" src="https://images.unsplash.com/photo-1586611292717-f828b167408c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" alt="Background" />
                </div>
            </div>

        </div>


    )
}
