
import { Link } from "react-router-dom";
import { usePostRoomMutation } from "../../app/room-api";
import { Room } from "../../entities";


export default function RoomDetailsv3(props: { room: Room; }) {

  function getSrc(picture: any) {
    if (!picture) {
      return 'https://via.placeholder.com/600'
    }
    else if (picture.startsWith('http')) {

      return picture

    }
    else return process.env.REACT_APP_SERVER_URL + picture

  }

  const { room } = props;

  return (



    <Link to={"/room/" + room!.id}>

      <div className="relative overflow-hidden transition duration-200 transform rounded shadow-lg hover:-translate-y-2 hover:shadow-2xl">
        <img
          className="object-cover w-full h-56 md:h-64 xl:h-80"
          src={getSrc(room.picture)}

        />
        <div className="absolute inset-0 px-6 py-4 transition-opacity duration-200 bg-black bg-opacity-75 opacity-0 hover:opacity-100">
          <p className="mb-4 text-lg font-bold text-gray-100">{room.name}</p>
          <p className="text-sm tracking-wide text-gray-300">
            {room.description}
          </p>

        </div>

      </div>
    </Link>



  )
}
