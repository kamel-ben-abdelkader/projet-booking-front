import { useGetAllRoomsQuery } from "../../app/room-api";
import { Room } from "../../entities";
import RoomDetailsv1 from "./RoomDetailsv1";
import RoomDetailsv2 from "./RoomDetailsv2";



export default function RoomLists() {

    const { data, isLoading, isError } = useGetAllRoomsQuery(); 
    return(
        <>
            
 {data?.map((item: Room, i) => (i%2 == 0 ? 

            <RoomDetailsv1 key={item.id} room={item} /> : 
            <RoomDetailsv2 key={item.id} room={item} /> 
 ))}
                    
        </>
    )
}
