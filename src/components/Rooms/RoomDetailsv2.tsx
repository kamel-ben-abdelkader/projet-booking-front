
import { Link } from "react-router-dom";
import { usePostRoomMutation } from "../../app/room-api";
import { Room } from "../../entities";




export default function RoomDetailsv2(props: { room: Room; }) {


  function getSrc(picture: any) {
    if (!picture) {
      return 'https://via.placeholder.com/600'
    }
    else if (picture.startsWith('http')) {

      return picture

    }
    else return process.env.REACT_APP_SERVER_URL + picture

  }


  const { room } = props;

  return (



    <div className="py-12 md:px-20 sm:px-14 px-6">


      <div className="sm:flex items-center shadow-md mt-10 md:flex flex-row">
        <div className="md: basis-1/2">
          <img className="object-cover" src={getSrc(room.picture)} />
        </div>
        <div className="md:px-10 sh sm:px-5 md:basis-1/2">
          <h1 className="text-gray-800 font-bold text-2xl my-2">{room.name}</h1>
          <p className="text-gray-700 mb-2 md:mb-6">{room.description}</p>
          <div className="flex justify-between mb-2">
            <span className=" text-sm font-bold text-indigo-500 font-medium title-font"></span>
            <Link to={'/room/' + room.id}>

              <p className="
               inline-block
               py-2
               px-7
               border border-[#E5E7EB]
               rounded-full
               text-base text-body-color
               bg-blue-500
               font-medium
               hover:border-primary hover:bg-primary hover:text-white
               transition
               ">Plus de Détails</p>
            </Link>

          </div>
        </div>
      </div>
    </div>



  )
}
