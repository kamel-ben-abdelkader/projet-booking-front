import { useState } from "react";
import { Navigate } from "react-router";
import { useAppDispatch } from "../app/hooks";
import { useGetSearchedRoomQuery} from "../app/room-api";



export default function SearchBar() {

 
    // const [search, setSearch] = useState<User>({} as User);
    let dispatch = useAppDispatch()



    const [searchedVal, setSearchedVal] = useState("")
   const { data, isLoading, isError } = useGetSearchedRoomQuery(searchedVal);
      const handleChange = (event: any) => {
          let value = event.target.value
          setSearchedVal(value);
      }
    
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        // const { data, isLoading, isError } = useGetSearchedRoomQuery(searchedVal);

        if (data) {
         
                console.log(data);
                
            }
            
        }
    


    // const [searchedVal, setSearchedVal] = useState("")
    // const { data, isLoading, isError } = useGetSearchedRoomQuery(searchedVal);
    // const handleChange = (event: any) => {
    //     let value = event.target.value
    //     setSearchedVal(value);
    // }


    // if (isLoading) {
    //     return <p>Loading...</p>
    // }
    // if (isError) {
    //     return <p>{isError}</p>
    // }



    //     setSearch(change)
    // }



    return (
<div className="overflow-hidden relative flex items-center min-h-screen">
  <div className="absolute bg-gradient-to-tr from-gray-800 via-gray-800 to-blue-500 w-full h-1/2 z-0 top-0" ></div>
  
  
  <div className="absolute inset-x-auto w-full z-10">
    <div className="w-2/3 mx-auto shadow-md rounded-md p-4 bg-white">
    <form  >
      <div className="flex gap-2 flex-col md:flex-row center">
        <div className="relative flex-1">
          <input id="departure" name="departure" type="text" className="peer h-10 w-full border border-1.5 rounded-md border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-indigo-600  focus:border-2 p-3" placeholder="quelquechose"  onChange={handleChange}/>
          <label  className="absolute left-2 px-1 -top-2.5 bg-white text-indigo-600  text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-900 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-red-600 peer-focus:text-sm">Lieu :</label>
        </div>
      
{/*    
        <div className="relative flex-1">
          <input id="eta" value="20/07/2021 à 06h" name="eta" type="text" className="peer h-10 w-full border border-1.5 rounded-md border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-red-600 focus:border-2 p-3" placeholder="quelquechose" />
          <label  className="absolute left-2 px-1 -top-2.5 bg-white text-red-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-900 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-red-600 peer-focus:text-sm">Date aller</label>
          <div className="absolute right-0 top-0 mt-2 mr-2">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
            </svg>
          </div>
        </div>
        <div className="relative flex-1">
          <input id="etd" name="etd" type="text" className="peer h-10 w-full border border-1.5 rounded-md border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-red-600 focus:border-2 p-3" placeholder="quelquechose" />
          <label  className="absolute left-2 px-1 -top-2.5 bg-white text-red-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-900 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-red-600 peer-focus:text-sm">Date retour</label>
          <div className="absolute right-0 top-0 mt-2 mr-2">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
            </svg>
          </div>
        </div> */}
      </div>
      <div className="flex justify-center mt-6">
        <button  className="bg-indigo-600  text-white font-extrabold text-lg rounded-full px-6 py-3">Rechercher</button>
      </div>
      </form>
    </div>
  </div>


{/* 
  <div date-rangepicker="" className="flex items-center">
<div className="relative">
<div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
<svg className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path></svg>
</div>
<input name="start" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 datepicker-input" placeholder="Select date start"/>
</div>
<span className="mx-4 text-gray-500">to</span>
<div className="relative">
<div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
<svg className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path></svg>
</div>
<input name="end" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 datepicker-input" placeholder="Select date end"/>
</div>
</div> */}


</div>


 )
}
