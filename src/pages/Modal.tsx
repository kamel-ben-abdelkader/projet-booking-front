import { useState } from 'react'
import { Booking } from '../entities';
import { Link } from 'react-router-dom';

export default function Modal(props: { booking: Booking; }) {
  const [showModal, setShowModal] = useState(false);
  const { booking } = props;


function TotalCount(date1: Date, date2 :Date, prix:number){

let Diff_temps = date2.getTime() - date1.getTime(); 
let Diff_jours = Diff_temps / (1000 * 3600 * 24); 
let Nombre_jours =  Math.round(Diff_jours)
let total =  Nombre_jours * prix

return  total
} 

function TotalDay(date1: Date, date2 :Date){

  let Diff_temps = date2.getTime() - date1.getTime(); 
  let Diff_jours = Diff_temps / (1000 * 3600 * 24); 
  let Nombre_jours =  Math.round(Diff_jours)
  return    Nombre_jours
  } 

  return (
    
    <>
    <button onClick={() => setShowModal(true)} className="flex ml-auto text-white bg-indigo-800 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded">Disponibilité</button>

    {showModal ? (

      <>
      
   <div className="flex justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
       
       
        <div className="w-1/3 p-4 md:p-4 bg-neutral-100/90">
        <button onClick={() => setShowModal(false)} type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex float-right dark:hover:bg-gray-800 dark:hover:text-white" data-modal-toggle="authentication-modal">
                  <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                </button>
            <h2 className="text-2xl font-bold text-gray-800 dark:text-white">Date arrivée : {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.arrivalDate!))}</h2>
            {/* <p className="mt-2 text-sm text-gray-600 dark:text-gray-400">{new Intl.DateTimeFormat('fr-FR').format(new Date(booking.arrivalDate!))}</p> */}

            <h2 className="text-2xl font-bold text-gray-800 dark:text-white">Date Départ : {new Intl.DateTimeFormat('fr-FR').format(new Date(booking.departureDate!))}</h2>
      
            <h1 className="text-lg font-bold text-gray-700 dark:text-gray-200 md:text-xl"> Nombre de nuits : {TotalDay(new Date(booking.arrivalDate!), new Date(booking.departureDate!))}</h1>
            <div className="flex justify-between mt-3 item-center">
                <h1 className="text-lg font-bold text-gray-700 dark:text-gray-200 md:text-xl">Total : {TotalCount(new Date(booking.arrivalDate!), new Date(booking.departureDate!), booking.customizedPrice!)} €</h1>

                <Link to={'/paiement/' + booking.id}>
                <button className="px-2 py-1 text-xs font-bold text-white uppercase transition-colors duration-200 transform bg-indigo-800 rounded dark:bg-gray-700 hover:bg-indigo-500 dark:hover:bg-indigo-800 focus:outline-none focus:bg-gray-700 dark:focus:bg-gray-600">Réserver</button>
                </Link>
            </div>
        </div>
    </div>



    
      </>
    ) : null}

  </>
);
}