import { useParams } from "react-router";
import { useGetOneHotelQuery } from "../app/hotel-api";
import { useGetOneRoombyHotelIdQuery } from "../app/room-api";
import { Room } from "../entities";
import RoomDetailsv3 from "../components/Rooms/RoomDetailsv3";



export default function HotelOnePage() {

    const { id } = useParams();

    const hotelQuery = useGetOneHotelQuery(Number(id));
    const roomQuery = useGetOneRoombyHotelIdQuery(Number(id));

    function getSrc(picture: any) {
        if (!picture) {
            return 'https://via.placeholder.com/600'
        }
        else if (picture.startsWith('http')) {

            return picture

        }
        else return process.env.REACT_APP_SERVER_URL + picture

    }

    return (
        <>
            <div className="w-full bg-center bg-cover h-[20rem]" style={{
                backgroundImage: `url(${hotelQuery.data?.picture!.startsWith('http') ? hotelQuery.data?.picture : process.env.REACT_APP_SERVER_URL! + hotelQuery.data?.picture})`,
            }}>
                <div className="flex items-center justify-center w-full h-full bg-gray-900 bg-opacity-50">
                    <div className="text-center">
                        <h1 className="text-2xl font-semibold text-white uppercase lg:text-5xl">{hotelQuery.data?.name}</h1>
                        <span className="flex items-center justify-center">
                            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-yellow-500" viewBox="0 0 24 24">
                                <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
                            </svg>
                            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-yellow-500" viewBox="0 0 24 24">
                                <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
                            </svg>
                            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-yellow-500" viewBox="0 0 24 24">
                                <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
                            </svg>
                            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-yellow-500" viewBox="0 0 24 24">
                                <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
                            </svg>
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-yellow-500" viewBox="0 0 24 24">
                                <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
                            </svg>
                            <span className="text-white ml-3">3 avis</span>
                        </span>
                        {/* <button className="w-full px-4 py-2 mt-4 text-sm font-medium text-white uppercase transition-colors duration-200 transform bg-blue-600 rounded-md lg:w-auto hover:bg-blue-500 focus:outline-none focus:bg-blue-500">Start project</button> */}
                    </div>
                </div>

            </div>

            <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                <div className="flex flex-col lg:flex-row">
                    <div className="max-w-xl pr-16 mx-auto mb-10">
                        <h5 className="mb-6 text-3xl font-extrabold leading-none">
                            Description :
                        </h5>
                        <p className="mb-6 text-gray-900">
                            {hotelQuery.data?.description}
                        </p>
                    </div>
                    <div className="grid gap-5 row-gap-5 sm:grid-cols-2">
                        <div className="max-w-md">
                            <div className="flex items-center justify-center w-16 h-16 mb-4 rounded-full bg-indigo-50">
                                <svg className="h-8 w-8 text-blue-700" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M3 7v11m0 -4h18m0 4v-8a2 2 0 0 0 -2 -2h-8v6" />  <circle cx="7" cy="10" r="1" /></svg>
                            </div>
                            <h6 className="mb-2 font-semibold leading-5">Services :</h6>
                            <p className="text-sm text-gray-700">
                                {hotelQuery.data?.service}
                            </p>
                        </div>
                        <div className="max-w-md">
                            <div className="flex items-center justify-center w-16 h-16 mb-4 rounded-full bg-indigo-50">
                                <svg className="h-8 w-8 text-blue-700" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                            </div>
                            <h6 className="mb-2 font-semibold leading-5">Adresse :</h6>
                            <p className="text-sm text-gray-700">
                                {hotelQuery.data?.adress}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="grid gap-6 row-gap-5 mb-8 lg:grid-cols-4 sm:row-gap-6 sm:grid-cols-2">

                    {roomQuery.data?.map((item: Room) => (
                        <RoomDetailsv3 key={item.id} room={item} />
                    ))}
                </div>
            </div>
        </>
    )
}
