import PresentationSection from "../components/Home/PresentationSection";
import Testimonial from "../components/Testimonial";
import HomeRoomsList from "../components/Home/HomeRoomsList";
import SearchBar from "../components/SearchBar";
import Faq from "../components/Home/Faq";
import StepUser from "../components/Faq/StepUser";
import StepPro from "../components/Faq/StepPro";



export default function HomePage() {



  return (
    <div>

      <PresentationSection />
      {/* <SearchBar /> */}
      <Testimonial />
      <HomeRoomsList />
      <Faq />


    </div>
  );
}
