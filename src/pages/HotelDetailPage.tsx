import { useParams } from "react-router";
import { useGetOneHotelQuery } from "../app/hotel-api";
import { useGetOneRoombyHotelIdQuery } from "../app/room-api";
import { Room } from "../entities";
import RoomDetailsv1 from "../components/Rooms/RoomDetailsv1";
import HotelHead from "../components/Hotels/HotelHead";



export default function HotelDetailPage() {

    const { id } = useParams();


    const hotelQuery = useGetOneHotelQuery(Number(id));

    const roomQuery = useGetOneRoombyHotelIdQuery(Number(id));

    return (

        <div>

            {roomQuery.data?.map((item: Room) => (
                <><HotelHead hotel={item} />

                    <RoomDetailsv1 key={item.id} room={item} /></>

            ))}

        </div>
    )
}
