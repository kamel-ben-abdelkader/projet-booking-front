import { useAppSelector } from '../app/hooks'
import HotelTables from '../components/Dashboard/HotelTables'
import { useGetHotelsbyUserIdQuery } from '../app/hotel-api'
import StepPro from '../components/Faq/StepPro'
import RoomTables from '../components/Dashboard/RoomTables'
import BookingTables from '../components/Dashboard/BookingTables'
import { Navigate } from 'react-router-dom'





function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

export default function DashboardAdmin() {

  let user = useAppSelector(state => state.auth.user)


  const { data } = useGetHotelsbyUserIdQuery(Number(user?.id));
  return (
    <>
      {user?.role!=='admin' && <Navigate to='/login' />}
      {/*
        This example requires updating your template:

        ```
        <html class="h-full bg-gray-100">
        <body class="h-full">
        ```
      */}
      <div className="min-h-full">

        <header className="bg-white shadow">
          <div className="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h1 className="text-3xl font-bold text-gray-900">Dashboard Admin</h1>
          </div>
        </header>
        <main>
          <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
            {/* Replace with your content */}

            {/* {data ===[''] ?   <HotelTables /> : <p>hello </p>} */}
            {/* {console.log("test " + JSON.stringify(data))} */}
            <StepPro />

            <HotelTables />
            <RoomTables />
            <BookingTables />

          </div>
        </main>
      </div>
    </>
  )
}
