import { Link } from "react-router-dom"
import {useAppSelector } from "../app/hooks"



export default function ProfilePage() {
    let user = useAppSelector(state => state.auth.user)




    return (


        <div className="w-full relative mt-4 shadow-2xl rounded my-24 overflow-hidden">
            <div className="top h-64 w-full bg-blue-600 overflow-hidden relative" >
                <img src="https://images.unsplash.com/photo-1503264116251-35a269479413?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" className="bg w-full h-full object-cover object-center absolute z-0" />
                <div className="flex flex-col justify-center items-center relative h-full bg-black bg-opacity-50 text-white">
                    <img src="https://business.ucr.edu/sites/g/files/rcwecm2116/files/styles/form_preview/public/icon-individual.png?itok=wbbnrR9I" className="h-24 w-24 object-cover rounded-full" />
                    <h4 className="text-sm font-semibold">Bienvenue</h4>
                    <h2 className="text-2xl font-semibold">{user?.firstname} {user?.name}</h2>
                </div>
            </div>
            <div className="grid grid-cols-12 bg-white ">

                <div className="col-span-12 w-full px-3 py-6 justify-center flex space-x-4 border-b border-solid md:space-x-0 md:space-y-4 md:flex-col md:col-span-2 md:justify-start ">
                    <Link to="/profile">
                        <p className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200" >
                            Mes Informations
                        </p>
                    </Link>
                    <Link to="/custom-profile">
                        <p className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200" >
                            Modifier mes informations
                        </p>
                    </Link>



                </div>

                <div className="col-span-12 md:border-solid md:border-l md:border-black md:border-opacity-25 h-full pb-12 md:col-span-10">
                    <div className="px-4 pt-4">
                        <form action="#" className="flex flex-col space-y-8">

                            <div>
                                <h3 className="text-2xl font-semibold">Mes Informations</h3>
                                <hr />
                            </div>


                            <div className="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4">

                                <div className="form-item w-full">
                                    <label className="text-xl ">Nom</label>
                                    <input type="text" value={user?.name} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled />
                                </div>

                                <div className="form-item w-full">
                                    <label className="text-xl ">Prénom</label>
                                    <input type="text" value={user?.firstname} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled />
                                </div>
                            </div>

                            <div className="form-item">
                                <label className="text-xl ">Adresse</label>
                                <input type="text" value={user?.adress} className="w-full appearance-none text-black rounded shadow py-1 px-2  mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200" disabled />
                            </div>



                            <div className="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4">

                                <div className="form-item w-full">
                                    <label className="text-xl ">Email</label>
                                    <input type="text" value={user?.email} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled />
                                </div>

                                <div className="form-item w-full">
                                    <label className="text-xl ">Tél</label>
                                    <input type="text" value={user?.phone} className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled />
                                </div>
                            </div>

                            {/*     
              <div>
                <h3 className="text-2xl font-semibold">My Social Media</h3>
                <hr/>
              </div>
    
              <div className="form-item">
                <label className="text-xl ">Instagram</label>
                <input type="text" value="https://instagram.com/" className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled/>
              </div>
              <div className="form-item">
                <label className="text-xl ">Facebook</label>
                <input type="text" value="https://facebook.com/" className="w-full appearance-none text-black rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 " disabled/>
              </div>
              <div className="form-item">
                <label className="text-xl ">Twitter</label>
                <input type="text" value="https://twitter.com/" className="w-full appearance-none text-black rounded shadow py-1 px-2  mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200  " disabled/>
              </div> */}

                        </form>
                    </div>
                </div>


            </div>
        </div>
    )
}