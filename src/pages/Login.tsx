import { useState } from "react";
import { Link } from "react-router-dom";
import { setCredentials } from "../app/auth-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { usePostLoginUserMutation } from "../app/user-api";
import { useNavigate } from 'react-router-dom'
import { User } from "../entities";


export default function Login() {

    const navigate = useNavigate()
    let user = useAppSelector(state => state.auth.user)

    const [postLogin, postQuery] = usePostLoginUserMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        const data = await postLogin(form).unwrap()

        if (data) {
            dispatch(setCredentials(data))
            if (data.user?.role === 'user') {
                navigate('/dashboard')
            }
            if (data.user?.role === 'admin') {
                navigate('/dashboard-pro')
            }
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }


    return (!user ?

        <div className="bg-white font-family-karla h-screen">

            <div className="w-full flex flex-wrap">

                <div className="w-full md:w-1/2 flex flex-col">

                    <div className="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-12">
                        <a href="#" className="bg-black text-white font-bold text-xl p-4">L B</a>
                    </div>

                    <div className="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
                        <p className="text-center text-3xl">Se connecter</p>
                        <form className="flex flex-col pt-3 md:pt-8" onSubmit={handleSubmit} >

                            <div className="flex flex-col pt-4">
                                <label htmlFor="email" className="text-lg">Email</label>
                                <input
                                    type="text"
                                    name="email"
                                    id="email"
                                    autoComplete="email"
                                    placeholder="your@email.com"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="flex flex-col pt-4">
                                <label htmlFor="password" className="text-lg">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    id="password"
                                    name="password"
                                    placeholder="Password"
                                    autoComplete="current-password"
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                                    required
                                    onChange={handleChange}
                                />
                            </div>

                            <input type="submit"
                                value="Se Connecter"
                                className="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8"

                            />
                        </form>

                    </div>

                </div>


                <div className="w-1/2 shadow-2xl">
                    <img className="object-cover w-full h-screen hidden md:block" src="https://images.unsplash.com/photo-1586611292717-f828b167408c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" alt="Background" />
                </div>
            </div>

        </div> : <Link to='/dashboard' />
    );
}

