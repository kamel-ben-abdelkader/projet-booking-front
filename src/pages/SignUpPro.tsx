import { useState } from "react";
import { setCredentials } from "../app/auth-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { usePostLoginUserMutation, usePostUserMutation } from "../app/user-api";
import { User } from "../entities";
import { Link, useNavigate } from 'react-router-dom'



export default function SignUpPro() {

  const navigate = useNavigate();

  let user = useAppSelector(state => state.auth.user)

  const [postUser] = usePostUserMutation();
  const [postLogin] = usePostLoginUserMutation();
  const [form, setForm] = useState<User>({} as User);
  let dispatch = useAppDispatch()

  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    form.role = 'admin'
    const data = await postUser(form).unwrap()

    if (data) {

      dispatch(setCredentials(data))

    }
    const dataLogin = await postLogin(form).unwrap()
    if (dataLogin) {
      dispatch(setCredentials(dataLogin))
      navigate('/dashboard-pro')
    }
  }

  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }
    setForm(change)
  }



  return (
    <>
      {!user ?
        <div className="mt-10 sm:mt-0">
          <div className="md:grid md:grid-cols-3 md:gap-6">
            <div className="md:col-span-1">

              <div className="px-4 sm:px-0max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                <h3 className="text-lg font-medium leading-6 text-gray-900"> Information d'inscription</h3>
                <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                  <span className="block">Si vous etes un particulier? </span>
                  <span className="block text-indigo-600">Veuillez vous inscrire par ici</span>
                </h2>


                <Link to="/signup">
                  <button type="button" className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700" >
                    Inscription Particulier
                  </button>
                </Link>
              </div>
            </div>
            <div className="mt-5 md:mt-0 md:col-span-2">
              <form onSubmit={handleSubmit} >
                <div className="shadow overflow-hidden sm:rounded-md">
                  <div className="px-4 py-5 bg-white sm:p-6">
                    <div className="grid grid-cols-6 gap-6">
                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                          Prénom
                        </label>
                        <input
                          type="text"
                          name="firstname"
                          id="firstname"
                          autoComplete="given-name"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                          Nom
                        </label>
                        <input
      
                          type="text"
                          name="name"
                          id="name"
                          autoComplete="family-name"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                          Nom société
                        </label>
                        <input
                          value="Hotel Simplon"
                          type="text"
                          name="company"
                          id="company"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                          Numéro Siret
                        </label>
                        <input
                          id="siret"
                          value="1451 962 587 00010"
                          type="text"
                          name="siret"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                          Email
                        </label>
                        <input
                          type="text"
                          name="email"
                          id="email" 
                          autoComplete="email"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                          Password
                        </label>
                        <input
                          autoComplete="current-password"
                          id="password"
                          type="password"
                          name="password"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-4">
                        <label htmlFor="address" className="block text-sm font-medium text-gray-700">
                          Adresse
                        </label>
                        <input
                          type="text"
                          name="adress"
                          id="adress"
                          autoComplete="adress"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>

                      <div className="col-span-6 sm:col-span-3">
                        <label htmlFor="phone" className="block text-sm font-medium text-gray-700">
                          Tel
                        </label>
                        <input
                          type="text"
                          name="phone"
                          value="0701035587"
                          id="phone"
                          autoComplete="phone"
                          className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          required
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                    <button
                      type="submit"
                      className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"

                    >
                      Enregister
                    </button>
                  </div>
                  <div className="text-center pt-12 pb-12">
                    <p>Déjà inscrit? <a href="login" className="underline font-semibold">Cliquez ICI.</a></p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div> : <div>Vous etes déjà connecté {user.firstname}</div>}

    </>
  );
}
