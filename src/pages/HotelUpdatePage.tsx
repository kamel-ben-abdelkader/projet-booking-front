import { useState } from "react";
import { useNavigate, useParams } from "react-router";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { hotelAPI, useGetOneHotelQuery, usePatchHotelMutation } from "../app/hotel-api";
import { Hotel } from "../entities";

export const HotelUpdatePage = () => {

  const { id } = useParams();
  const hotelQuery = useGetOneHotelQuery(Number(id));


  const navigate = useNavigate()
  let user = useAppSelector(state => state.auth.user)
  const dispatch = useAppDispatch();

  const [postHotel, postQuery] = usePatchHotelMutation()
  const [form, setForm] = useState<Hotel>({} as Hotel);


  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
      event.preventDefault();
      try {
          await postHotel({ body: form, id: hotelQuery?.data?.id! }).unwrap();
          alert("Vos modifications sont enregistrées")
          dispatch(hotelAPI.util.invalidateTags(['HotelList']))
          navigate('/dashboard-pro')

      } catch (error) {
          event.preventDefault();
          alert("Vos modifications sont erronnées, recommencez")
          console.log(error);

      }

  }
  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }


    setForm(change)
}

    return (
      <div className="relative">
              <img
          src={hotelQuery.data?.picture!.startsWith('http') ? hotelQuery.data?.picture : process.env.REACT_APP_SERVER_URL! + hotelQuery.data?.picture}
          className="absolute inset-0 object-cover w-full h-full"
          alt=""
        />
        <div className="relative bg-gray-900 bg-opacity-75">
          <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
            <div className="flex flex-col items-center justify-between xl:flex-row">
              <div className="w-full max-w-xl mb-12 xl:mb-0 xl:pr-16 xl:w-7/12">
                <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-cyan-500 sm:text-4xl sm:leading-none">
                Information actuel sur l'hotel : {hotelQuery.data?.name}<br className="hidden md:block" />
            
                </h2>
                <p className="max-w-xl mb-4 text-base text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Nom : </span>{hotelQuery.data?.name}
                </p>
                <p className="max-w-xl mb-4 text-base text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Description : </span>{hotelQuery.data?.description}
                </p>
                <p className="max-w-xl mb-4 text-base text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Adresse : </span>{hotelQuery.data?.adress}
                </p>
                <p className="max-w-xl mb-4 text-base text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Service: </span>{hotelQuery.data?.service}
                </p>
                <p className="max-w-xl mb-4 text-base text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Type : </span>{hotelQuery.data?.type}
                </p>
              </div>
              <div className="w-full max-w-xl xl:px-8 xl:w-5/12">
                <div className="bg-white rounded shadow-2xl p-7 sm:p-10">
                  <h3 className="mb-4 text-xl font-semibold sm:text-center sm:mb-6 sm:text-2xl">
                    Remplissez le ou les champs que vous souhaitez modifier
                  </h3>
                  <form action="#" method="POST" onSubmit={handleSubmit}>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="name"
                        className="inline-block mb-1 font-medium"
                      >
                        Nom
                      </label>
                      <input
                        placeholder={hotelQuery.data?.name}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="name"
                        name="name"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="description"
                        className="inline-block mb-1 font-medium"
                      >
                        Description
                      </label>

                      <textarea
      className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
      id="description"
      name="description"
      onChange={handleChange}
    ></textarea>
                      {/* <input
  
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="description"
                        name="description"
                        onChange={handleChange}
                      /> */}
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="adress"
                        className="inline-block mb-1 font-medium"
                      >
                        Adresse
                      </label>
                      <input
                        placeholder={hotelQuery.data?.adress}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="adress"
                        name="adress"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="service"
                        className="inline-block mb-1 font-medium"
                      >
                        Service
                      </label>
                      <input
                        placeholder={hotelQuery.data?.service}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="service"
                        name="service"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="type"
                        className="inline-block mb-1 font-medium"
                      >
                        Type
                      </label>
                      <input
                        placeholder={hotelQuery.data?.type}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="type"
                        name="type"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mt-4 mb-2 sm:mb-4">
                      <button
                        type="submit"
                        className="               
                        inline-block
                        py-2
                        px-7
                        border border-[#E5E7EB]
                        rounded-full
                        text-base text-body-color
                        bg-blue-500
                        font-medium
                        hover:border-primary hover:bg-primary hover:text-white
                        transition"
                      >
                        Sauvegardez
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };