import { useState } from "react"
import { Link } from "react-router-dom"
import { useAppDispatch, useAppSelector } from "../app/hooks"
import { usePatchUserMutation, userAPI } from "../app/user-api"
import { User } from "../entities"
import { useNavigate } from 'react-router-dom'


export default function CustomProfilePage() {

    const navigate = useNavigate()
    let user = useAppSelector(state => state.auth.user)
    const dispatch = useAppDispatch();

    const [postUser, postQuery] = usePatchUserMutation()
    const [form, setForm] = useState<User>({} as User);


    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        try {
            await postUser({ body: form, id: user?.id! }).unwrap();
            alert("Vos modifications sont enregistrées")
            dispatch(userAPI.util.invalidateTags(['UserList']))
            navigate('/profile')

        } catch (error) {
            event.preventDefault();
            alert("Vos modifications sont erronnées, recommencez")
            console.log(error);

        }

    }
    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }

    return (

        <div className="w-full relative mt-4 shadow-2xl rounded my-24 overflow-hidden">
            <div className="top h-64 w-full bg-blue-600 overflow-hidden relative" >
                <img src="https://images.unsplash.com/photo-1503264116251-35a269479413?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" alt="" className="bg w-full h-full object-cover object-center absolute z-0" />
                <div className="flex flex-col justify-center items-center relative h-full bg-black bg-opacity-50 text-white">
                <img src="https://business.ucr.edu/sites/g/files/rcwecm2116/files/styles/form_preview/public/icon-individual.png?itok=wbbnrR9I" className="h-24 w-24 object-cover rounded-full" />
                    <h4 className="text-sm font-semibold">Bienvenue</h4>
                    <h2 className="text-2xl font-semibold">{user?.firstname} {user?.name}</h2>
                    {console.log(user)}
                </div>
            </div>
            <div className="grid grid-cols-12 bg-white ">

                <div className="col-span-12 w-full px-3 py-6 justify-center flex space-x-4 border-b border-solid md:space-x-0 md:space-y-4 md:flex-col md:col-span-2 md:justify-start ">
                    <Link to="/profile">
                        <p className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200" >
                            Mes Informations
                        </p>
                    </Link>
                    <Link to="/custom-profile">
                        <p className="text-sm p-2 bg-indigo-200 text-center rounded font-semibold hover:bg-indigo-700 hover:text-gray-200" >
                            Modifier mes informations
                        </p>
                    </Link>

                </div>

                <div className="col-span-12 md:border-solid md:border-l md:border-black md:border-opacity-25 h-full pb-12 md:col-span-10">
                    <div className="px-4 pt-4">
                        <form className="flex flex-col space-y-8" action="#" method="POST" onSubmit={handleSubmit}>

                            <div>
                                <h3 className="text-2xl font-semibold">Mes Informations</h3>
                                <hr />
                            </div>


                            <div className="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4">

                                <div className="form-item w-full">
                                    <label className="text-xl ">Nom</label>
                                    <input type="text"
                                        className="w-full appearance-none text-black text-opacity-50 rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 text-opacity-25 "
                                        name="name"
                                        placeholder={user?.name}
                                        onChange={handleChange}
                                    />
                                </div>

                                <div className="form-item w-full">
                                    <label className="text-xl ">Prénom</label>
                                    <input type="text"
                                        className="w-full appearance-none text-black text-opacity-50 rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 text-opacity-25 "
                                        name="firstname"
                                        placeholder={user?.firstname}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>

                            <div className="form-item">
                                <label className="text-xl ">Adresse</label>
                                <input type="text"
                                    className="w-full appearance-none text-black text-opacity-50 rounded shadow py-1 px-2  mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200"
                                    name="adress"
                                    placeholder={user?.adress}
                                    onChange={handleChange}
                                />
                            </div>



                            <div className="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-4">

                                <div className="form-item w-full">
                                    <label className="text-xl ">Email</label>
                                    <input type="text"
                                        className="w-full appearance-none text-black text-opacity-50 rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 text-opacity-25 "
                                        name="email"
                                        placeholder={user?.email}
                                        onChange={handleChange}
                                    />
                                </div>

                                <div className="form-item w-full">
                                    <label className="text-xl ">Tél</label>
                                    <input type="text"
                                        className="w-full appearance-none text-black text-opacity-50 rounded shadow py-1 px-2 mr-2 focus:outline-none focus:shadow-outline focus:border-blue-200 "
                                        name="phone"
                                        value={user?.phone}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>

                            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button
                                    type="submit"
                                    className="inline-flex justify-center py-2 px-4 text-white rounded-md bg-[#5f0f40] opacity-90 hover:opacity-100 hover:ring-2 focus:  ease-linear transition-all duration-150  "
                                >
                                    Enregistrer
                                </button>
                                {postQuery.isError && <p>{(postQuery.error as any).data.error}</p>
                                }
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    )
}