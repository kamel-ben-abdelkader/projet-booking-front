import { useState } from "react";
import { useNavigate, useParams } from "react-router";
import { bookingAPI, useGetOneBookingQuery, usePatchBookingMutation } from "../app/booking-api";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { hotelAPI } from "../app/hotel-api";
import {  useGetOneRoomQuery, usePatchRoomMutation } from "../app/room-api";
import { Booking} from "../entities";

export const BookingUpdatePage = () => {

  const { id } = useParams();
 
  const bookingQuery =  useGetOneBookingQuery(Number(id));

  const navigate = useNavigate()
  let user = useAppSelector(state => state.auth.user)
  const dispatch = useAppDispatch();


  
  const [postBooking, postQuery] =  usePatchBookingMutation()
  const [form, setForm] = useState<Booking>({} as Booking);

  const roomQuery = useGetOneRoomQuery(Number(bookingQuery.data?.roomId))


  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
      event.preventDefault();
      try {
          await postBooking({ body: form, id: bookingQuery?.data?.id! }).unwrap();
          alert("Vos modifications sont enregistrées")
          dispatch(bookingAPI.util.invalidateTags(['BookingList', 'BookingUser']))
          navigate('/dashboard-pro')

      } catch (error) {
          event.preventDefault();
          alert("Vos modifications sont erronnées, recommencez")
          console.log(error);

      }

  }
  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }


    setForm(change)
}

console.log(new Date(bookingQuery.data?.arrivalDate!));

return (
    
      <div className="relative">
              <img
          src={roomQuery.data?.picture!.startsWith('http') ? roomQuery.data?.picture : process.env.REACT_APP_SERVER_URL! + roomQuery.data?.picture}
          className="absolute inset-0 object-cover w-full h-full"
          alt=""
        />
        <div className="relative bg-gray-900 bg-opacity-75">
          <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
            <div className="flex flex-col items-center justify-between xl:flex-row">
              <div className="w-full max-w-xl mb-12 xl:mb-0 xl:pr-16 xl:w-7/12">

                <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-white  sm:text-4xl sm:leading-none">{roomQuery.data?.name}<br className="hidden md:block" />  
                </h2>
                <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-cyan-500  sm:text-4xl sm:leading-none">
                Information réservation :      
                </h2>


                <p className="max-w-xl mb-4 text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Date arrivée : </span> {bookingQuery.data?.arrivalDate!}
                </p>
                <p className="max-w-xl mb-4 text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Date de départ: </span> {bookingQuery.data?.departureDate!}
                </p>
                <p className="max-w-xl mb-4 text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Prix: </span>{bookingQuery.data?.customizedPrice} €
                </p>
              </div>
              <div className="w-full max-w-xl xl:px-8 xl:w-5/12">
                <div className="bg-white rounded shadow-2xl p-7 sm:p-10">
                  <h3 className="mb-4 text-xl font-semibold sm:text-center sm:mb-6 sm:text-2xl">
                    Remplissez le ou les champs que vous souhaitez modifier
                  </h3>
                  <form className="flex flex-col pt-3 md:pt-8" onSubmit={handleSubmit} >

<div className="flex flex-col pt-4">
    <label htmlFor="name" className="text-lg">Date arrivée</label>
    <input
        type="date"
        name="arrivalDate"
        id="arrivalDate"
        autoComplete="arrivalDate"
        placeholder="nom de l'Hotel"
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
        onChange={handleChange}
    />
</div>

<div className="flex flex-col pt-4">
    <label htmlFor="departureDate" className="text-lg">
        date depart
    </label>
    <input
        type="date"
        id="departureDate"
        name="departureDate"
        placeholder="departureDate"
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
        onChange={handleChange}
    />
</div>


<div className="flex flex-col pt-4">
    <label htmlFor="customizedPrice" className="text-lg">
        Prix de la nuit : 
    </label>
    <input
        type="number"
        id="customizedPrice"
        name="customizedPrice"
        placeholder="prix"
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
        onChange={handleChange}
    />
</div>


<div className="mt-4 mb-2 sm:mb-4">
                      <button
                        type="submit"
                        className="               
                        inline-block
                        py-2
                        px-7
                        border border-[#E5E7EB]
                        rounded-full
                        text-base text-body-color
                        bg-blue-500
                        font-medium
                        hover:border-primary hover:bg-primary hover:text-white
                        transition"
                      >
                        Sauvegardez
                      </button>
                    </div>
</form>
                 
               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };