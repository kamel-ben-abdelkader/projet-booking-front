import { useState } from "react";
import { useNavigate, useParams } from "react-router";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { hotelAPI } from "../app/hotel-api";
import { roomAPI, useGetOneRoomQuery, usePatchRoomMutation } from "../app/room-api";
import { Room } from "../entities";

export const RoomUpdatePage = () => {

  const { id } = useParams();
  const roomQuery = useGetOneRoomQuery(Number(id));

  const navigate = useNavigate()
  let user = useAppSelector(state => state.auth.user)
  const dispatch = useAppDispatch();

  const [postRoom, postQuery] =  usePatchRoomMutation()
  const [form, setForm] = useState<Room>({} as Room);


  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
      event.preventDefault();
      try {
          await postRoom({ body: form, id: roomQuery?.data?.id! }).unwrap();
          alert("Vos modifications sont enregistrées")
          dispatch(hotelAPI.util.invalidateTags(['HotelUser']))
          navigate('/dashboard-pro')

      } catch (error) {
          event.preventDefault();
          alert("Vos modifications sont erronnées, recommencez")
          console.log(error);

      }

  }
  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }


    setForm(change)
}

return (
    
      <div className="relative">
              <img
          src={roomQuery.data?.picture!.startsWith('http') ? roomQuery.data?.picture : process.env.REACT_APP_SERVER_URL! + roomQuery.data?.picture}
          className="absolute inset-0 object-cover w-full h-full"
          alt=""
        />
        <div className="relative bg-gray-900 bg-opacity-75">
          <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
            <div className="flex flex-col items-center justify-between xl:flex-row">
              <div className="w-full max-w-xl mb-12 xl:mb-0 xl:pr-16 xl:w-7/12">
                <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-cyan-500  sm:text-4xl sm:leading-none">
                Information actuel sur l'hotel : {roomQuery.data?.name}<br className="hidden md:block" />
            
                </h2>
                <p className="max-w-xl mb-4 text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Nom : </span>{roomQuery.data?.name}
                </p>
                <p className="max-w-xl mb-4 text-white md:text-lg ">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Description : </span>{roomQuery.data?.description}
                </p>
                <p className="max-w-xl mb-4 text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Ville : </span>{roomQuery.data?.location}
                </p>
                <p className="max-w-xl mb-4 text-white md:text-lg">
                <span className="text-teal-accent-400 font-bold text-cyan-500 ">Prix: </span>{roomQuery.data?.price} €
                </p>
              </div>
              <div className="w-full max-w-xl xl:px-8 xl:w-5/12">
                <div className="bg-white rounded shadow-2xl p-7 sm:p-10">
                  <h3 className="mb-4 text-xl font-semibold sm:text-center sm:mb-6 sm:text-2xl">
                    Remplissez le ou les champs que vous souhaitez modifier
                  </h3>
                  <form action="#" method="POST" onSubmit={handleSubmit}>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="name"
                        className="inline-block mb-1 font-medium"
                      >
                        Nom
                      </label>
                      <input
                        placeholder={roomQuery.data?.name}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="name"
                        name="name"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="description"
                        className="inline-block mb-1 font-medium"
                      >
                        Description
                      </label>

                      <textarea
      className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
      id="description"
      name="description"
      onChange={handleChange}
    ></textarea>
                      {/* <input
  
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="description"
                        name="description"
                        onChange={handleChange}
                      /> */}
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="adress"
                        className="inline-block mb-1 font-medium"
                      >
                       Ville
                      </label>
                      <input
                        placeholder={roomQuery.data?.location}
                        type="text"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="adress"
                        name="adress"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mb-1 sm:mb-2">
                      <label
                        htmlFor="service"
                        className="inline-block mb-1 font-medium"
                      >
                        Prix
                      </label>
                      <input
                        placeholder={roomQuery.data?.price}
                        type="number"
                        className="flex-grow w-full h-12 px-4 mb-2 transition duration-200 bg-white border border-gray-300 rounded shadow-sm appearance-none focus:border-deep-purple-accent-400 focus:outline-none focus:shadow-outline"
                        id="service"
                        name="service"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="mt-4 mb-2 sm:mb-4">
                      <button
                        type="submit"
                        className="               
                        inline-block
                        py-2
                        px-7
                        border border-[#E5E7EB]
                        rounded-full
                        text-base text-body-color
                        bg-blue-500
                        font-medium
                        hover:border-primary hover:bg-primary hover:text-white
                        transition"
                      >
                        Sauvegardez
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };