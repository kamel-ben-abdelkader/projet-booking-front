import { useParams } from "react-router";
import { useGetBookingsbyRoomIdQuery } from "../app/booking-api";
import { useGetOneHotelQuery } from "../app/hotel-api";
import { useGetOneRoomQuery } from "../app/room-api";
import BookingAvailablity from "../components/Bookings/BookingAvailibility";
import Availablity from "../components/Home/Availablity";
import StarRating from "../components/StarRating";
import { Booking, Room } from "../entities";
import Modal from "./Modal";




export default function RoomOnePage() {

  const { id } = useParams();


  const roomQuery = useGetOneRoomQuery(Number(id));
  const bookingQuery = useGetBookingsbyRoomIdQuery(Number(id));

  function getSrc(picture: any) {
    if (!picture) {
      return 'https://via.placeholder.com/600'
    }
    else if (picture.startsWith('http')) {

      return picture

    }
    else return process.env.REACT_APP_SERVER_URL + picture

  }
   
      return (

    <section className="text-gray-600 body-font overflow-hidden">
      <div className="container px-5 py-24 mx-auto">
        <div className="lg:w-4/5 mx-auto flex flex-wrap">
          <img className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded" src={getSrc(roomQuery?.data?.picture)} />
          <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
            <h2 className="text-sm title-font text-gray-500 tracking-widest">{roomQuery?.data?.location}</h2>
            <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">{roomQuery?.data?.name!}</h1>
            <div className="flex mb-4">
              <span className="flex items-center">
                <StarRating />
                {/* <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-indigo-500" viewBox="0 0 24 24">
              <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
            </svg>
            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-indigo-500" viewBox="0 0 24 24">
              <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
            </svg>
            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-indigo-500" viewBox="0 0 24 24">
              <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
            </svg>
            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-indigo-500" viewBox="0 0 24 24">
              <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
            </svg>
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 text-indigo-500" viewBox="0 0 24 24">
              <path d="M12 2l3.09 6.26L22 9.27l-5 4.87 1.18 6.88L12 17.77l-6.18 3.25L7 14.14 2 9.27l6.91-1.01L12 2z"></path>
            </svg>
            <span className="text-gray-600 ml-3">4 Reviews</span> */}
              </span>
              <span className="flex ml-3 pl-3 py-2 border-l-2 border-gray-200 space-x-2s">
                <a className="text-gray-500">
                  <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                    <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                  </svg>
                </a>
                <a className="text-gray-500">
                  <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                    <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                  </svg>
                </a>
                <a className="text-gray-500">
                  <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                    <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                  </svg>
                </a>
              </span>
            </div>
            <p className="leading-relaxed">{roomQuery?.data?.description!}</p>

            {/* {bookingQuery?.data?.map((item: Booking) => (

<Availablity key={item.id} booking={item} />
))} */}

            <div >
              <br />
              <p className="leading-relaxed">{bookingQuery?.data?.map((item: Booking) => (

                <BookingAvailablity key={item.id} booking={item} />
              ))}</p>
              <br />
              <p className="leading-relaxed">Au lieu de {roomQuery?.data?.price!}€/ Nuit</p>
            </div>
            
            {bookingQuery?.data?.map((item: Booking) => (

<Modal key={item.id} booking={item} />
))}

{/* {!bookingQuery.data == null ? bookingQuery?.data!.map((item: Booking)=> (

<Modal key={item.id} booking={item} />
)) :
<div className="flex ml-auto text-white bg-pink-800 border-0 py-2 px-6 focus:outline-none rounded">
  Aucune Offre actuellement</div>
} */}
      
          </div>
        </div>
      </div>
    </section>
  )
}
